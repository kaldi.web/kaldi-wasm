#!/bin/bash

# This script assumes that the clapack directory is at the same level as kaldi

LAPACK_DIR=${1:-'clapack-wasm'}

ORIGIN_DIR=$(pwd)
CXXFLAGS="-O3 -U HAVE_EXECINFO_H"
LDFLAGS="-O3"

cd kaldi/src
CXXFLAGS="$CXXFLAGS" LDFLAGS="$LDFLAGS" emconfigure ./configure --use-cuda=no \
    --static --clapack-root=../../"$LAPACK_DIR" --host=WASM
sed -i -e 's:-pthread::g; s:-lpthread::g' kaldi.mk
emmake make -j clean depend
emmake make -j $(nproc) online2bin

