#!/bin/bash

set -e
set -o nounset

PROGRAM=online2-tcp-nnet3-decode-faster-reorganized
WASM_NAME=kaldiJS

EM_OPTS="-s WASM=1 -s MODULARIZE=1 -s ENVIRONMENT='worker' -s BUILD_AS_WORKER=1 \
         -s EXPORT_NAME='kaldiJS' -s EXTRA_EXPORTED_RUNTIME_METHODS=['FS'] \
         -s INVOKE_RUN=0 -s ERROR_ON_UNDEFINED_SYMBOLS=0 -s TOTAL_MEMORY=300MB \
         -s ALLOW_MEMORY_GROWTH=1 --bind -lidbfs.js"

curr_dir="$(pwd)"
cd kaldi/src/online2bin
cp $PROGRAM $PROGRAM.bc
em++ $EM_OPTS -o $WASM_NAME.js $PROGRAM.bc
mkdir -p "$curr_dir/src/computations"
mv $WASM_NAME.* "$curr_dir/src/computations"
rm $PROGRAM.bc
cd "$curr_dir"
